package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

public interface IAuthService {

    void logout();

    void login(@Nullable String login, @Nullable String password);

    @NotNull
    String getUserId();

    boolean isLoggedIn();

    @NotNull
    User getUser();

    void checkRoles(@Nullable Role... roles);

}
